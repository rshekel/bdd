def expression_to_truth_table(expr):
    # TODO: make this work generically for n variables
    l = [0,0,0,0,0,0,0,0]
    SumOfProduct = ["(~a~b~c)","(~a~bc)","(~ab~c)","(~abc)","(a~b~c)","(a~bc)","(ab~c)","(abc)"]
    for i in range(8):
        if (expr.find(SumOfProduct[i]) >= 0):
            l[i] = 1

    # example of XOR for 4 variables
    # return [0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0]
    return l


class Node(object):
    def __init__(self, parents, layer_num, index, is_last=False):
        assert parents is None or isinstance(parents, list)
        if layer_num == 0:
            self.is_root = True
        else:
            self.is_root = False

        self.index = index
        self.parents = parents
        self.layer_num = layer_num
        self.false_child = None
        self.true_child = None
        self.is_last = is_last
        self.value = None
        self._li_str = "[{layer_num}{index}]".format(layer_num=self.layer_num, index=self.index)

    @property
    def fc(self):
        return self.false_child

    @property
    def tc(self):
        return self.true_child

    def print_node(self, with_parents=True):
        print(self._li_str + ":", end='')
        if with_parents:
            print("(", end='')
            if self.parents:
                for parent in self.parents:
                    parent.print_node(with_parents=False)
                    print(',', end='')
            print(')', end='')
        if self.is_last:
            print(':v{value}'.format(value=self.value), end='')

    def is_orphan(self):
        ans = True
        for h in self.get_histories():
            if h.startswith('r'):
                ans = False
        return ans

    def get_histories(self, reverse=True):
        """
        :param reverse - whether to reverse str at the end - you want to reverse only at the "end" of recursion,
        so the 'r' will be at beggining, and repr. will be from root down and not down-up
        returns string representing true/false choices from root till self
        """
        histories = []
        if self.is_root:
            return ['r']

        if self.parents is None:
            return []
        for p in self.parents:
            s1 = ''
            s0 = ''
            # I might be both a true_child and a false_child
            if p.true_child == self:
                s1 += '1'
            if p.false_child == self:
                s0 += '0'
            upper_histories = p.get_histories(reverse=False)
            ss1 = []
            ss0 = []
            if s1:
                ss1 = [s1 + upper_history for upper_history in upper_histories]
            if s0:
                ss0 = [s0 + upper_history for upper_history in upper_histories]
            ss = ss1 + ss0

            if reverse:
                ss = [s[::-1] for s in ss]
                self.assert_no_bad_histories(histories)
            histories += ss

        return histories

    def assert_no_bad_histories(self, histories):
        has_legal = False
        has_ilegal = False
        for hist in histories:
            # might be orphan wich is OK
            if not hist.startswith('r'):
                has_ilegal = True
            # means it has a normal ancestry
            if hist.startswith('r'):
                has_legal = True
        if has_ilegal and has_legal:
            assert False, "this node has a bug - both a history which goes to root, " \
                          "and a history which is redundant"

    def get_branch_represantation(self):
        # always go first to truth child (repr. by t), and if last - return value (0 or 1)
        if self.is_last:
            return str(self.value)
        else:
            s = ''
            s += 't'
            s += self.true_child.get_branch_represantation()
            s += 'f'
            s += self.false_child.get_branch_represantation()
        return s


# false comes first, so is the left choice on every branch, and first in index of every layer
class Tree(object):
    def __init__(self, logical_expression, layer_amount=3):
        self.logical_expression = logical_expression
        self.truth_table = expression_to_truth_table(self.logical_expression)

        self.layer_amount = layer_amount
        self.root = Node(None, 0, 0)
        self.layers = []
        # +1 for values layer (truth table)
        for i in range(layer_amount + 1):
            self.layers.append([])
        self.layers[0] = [self.root]
        self.build_tree()
        self.input_final_values()

    def build_tree(self):
        # Don't want to build children for last layer - which is the values layer...
        for layer in self.layers[:-1]:
            for j, node in enumerate(layer):
                self._build_children(node, j)

    def _build_children(self, node, j):
        child_layer = node.layer_num + 1
        node.false_child = Node([node], child_layer, index=2*j)
        node.true_child = Node([node], child_layer, index=2*j + 1)
        self.layers[child_layer] += [node.false_child, node.true_child]

    def input_final_values(self):
        for node in self.layers[-1]:
            node.is_last = True
            node.value = self.truth_table[node.index]

    def _print_layer(self, layer_num):
        current_layer = self.layers[layer_num]
        for node in current_layer:
            node.print_node()
            print('\t', end='')

    def print_tree(self):
        for layer_num in range(self.layer_amount + 1):
            self._print_layer(layer_num)
            print()

    def remove_redundant_test(self):
        for layer in self.layers:
            for node in layer:
                if not node.parents:
                    continue
                if node.is_last:
                    continue
                if node.true_child == node.false_child:
                    for parent in node.parents:
                        if parent.true_child == node:
                            parent.true_child = node.true_child
                        if parent.false_child == node:
                            parent.false_child = node.true_child
                        if parent not in node.true_child.parents:
                            node.true_child.parents.append(parent)
                    node.true_child.parents.remove(node)
                    node.parents = None
        self.remove_orphans()

    def merge_equivalent_branches(self):
        found_something = True
        while found_something:
            self.remove_orphans()
            found_something = False

            for layer in self.layers:
                reprs = [n.get_branch_represantation() for n in layer]
                for node in layer:
                    # if node doesn't have parents - it is either root, or an orphan and not interesting
                    if node.parents is None:
                        continue
                    this_repr = node.get_branch_represantation()
                    if reprs.count(this_repr) > 1:
                        found_something = True
                        # taking [1] for second instance with same branch architecture.
                        # we will always have node be the first one, and other_node be second one
                        other_node = [n for n in layer if n.get_branch_represantation() == this_repr][1]
                        for parent in node.parents:
                            if parent.false_child is node:
                                parent.false_child = other_node
                            # Not using else because it might be true for both
                            if parent.true_child is node:
                                parent.true_child = other_node
                            # To prevent having same father twice
                            if parent not in other_node.parents:
                                other_node.parents.append(parent)
                        node.parents = None
                        if node.true_child:
                            node.true_child.parents.remove(node)
                        if node.false_child:
                            node.false_child.parents.remove(node)
                        break
                if found_something:
                    break
        self.remove_orphans()

    def remove_orphans(self):
        for layer in self.layers:
            to_remove = []
            for node in layer:
                if node.is_orphan():
                    to_remove.append(node)
            for n in to_remove:
                layer.remove(n)


# Now using it
expr = "(~a~b~c)+(~a~bc)+(ab~c)"
expr2 = "(~a~bc)+(~ab~c)+(a~b~c)+(abc)"

t = Tree(expr2)
t.print_tree()
