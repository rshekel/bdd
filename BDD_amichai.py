import string
import re
#=====================================================================
#   expression_to_truth_table - gets the string input (bolean logic) into reurns a truth tabel
#=====================================================================
def n(x):
    return 1-x

def is_letter(ch):
    return ch.lower() in string.ascii_lowercase

def count_variables(bool_string):
    var_dict = {}
    for ch in bool_string:
        if is_letter(ch):
            if ch not in var_dict:
                var_dict[ch] = 1
            else:
                var_dict[ch] = var_dict[ch] + 1
    return var_dict

def replace_tild(s):
    s = s.replace('~(','n(')
    s = re.sub(r"~(\w)",r"n(\1)",s)
    return s

def zero_all_variables(var_dict, string):
    for var in var_dict:
        string = string.replace(var, '0')
    return string
    
def expression_to_truth_table(bool_string):
    truth_table = []
    var_dict = count_variables(bool_string)
    bool_string = replace_tild(bool_string)
    
    format_template = '0{0}b'.format(len(var_dict))
    for i in range(2**len(var_dict)):        
        cur_expr = bool_string
        bit_indicator = format(i, format_template)
        j=-1
        for var in var_dict:            
            cur_expr = cur_expr.replace(var,bit_indicator[j])
            j=j-1
        truth_table.append(eval(cur_expr))
    return truth_table, len(var_dict)

#=====================================================================
#   class Node: 
#=====================================================================
class Node(object):
    def __init__(self, parents, layer_num, index, is_last=False):
        assert parents is None or isinstance(parents, list)
        if layer_num == 0:
            self.is_root = True
        else:
            self.is_root = False

        self.index = index
        self.parents = parents
        self.layer_num = layer_num
        self.false_child = None
        self.true_child = None
        self.is_last = is_last
        self.value = None
        self._li_str = "x{layer_num}[{index}]".format(layer_num=self.layer_num, index=self.index)
    @property
    def fc(self):
        return self.false_child

    @property
    def tc(self):
        return self.true_child
        
    def print_node2(self, with_parents=True):
        if not self.is_last:
            print(self._li_str, end='')
            if with_parents:
                if self.parents:
                    for parent in self.parents:
                        print("^", end='')
                        parent.print_node2(with_parents=False)
        if self.is_last:
            print('final({value})'.format(value=self.value), end='')
            for parent in self.parents:
                print("^", end='')
                parent.print_node2(with_parents=False)
    def print_node(self):
        
        if not self.is_last:    
            print ("x{layer_num},".format(layer_num=self.layer_num+1), end='')
            if (self.false_child.value == None):
                print ("x{false_child},".format(false_child=self.false_child.layer_num+1), end='')
            else:
                print ("{value},".format(value=self.false_child.value), end='')
            if (self.true_child.value == None):
                print ("x{true_child}".format(true_child=self.true_child.layer_num+1), end='')
            else:
                print ("{value}".format(value=self.true_child.value), end='')
            print(';\t', end='')
        else:
            print('{value}--;\t'.format(value=self.value), end='')

    def is_orphan(self):
        ans = True
        for h in self.get_histories():
            if h.startswith('r'):
                ans = False
        return ans

    def get_histories(self, reverse=True):
        """
        :param reverse - whether to reverse str at the end - you want to reverse only at the "end" of recursion,
        so the 'r' will be at beggining, and repr. will be from root down and not down-up
        returns string representing true/false choices from root till self
        """
        histories = []
        if self.is_root:
            return ['r']

        if self.parents is None:
            return []
        for p in self.parents:
            s1 = ''
            s0 = ''
            # I might be both a true_child and a false_child
            if p.true_child == self:
                s1 += '1'
            if p.false_child == self:
                s0 += '0'
            upper_histories = p.get_histories(reverse=False)
            ss1 = []
            ss0 = []
            if s1:
                ss1 = [s1 + upper_history for upper_history in upper_histories]
            if s0:
                ss0 = [s0 + upper_history for upper_history in upper_histories]
            ss = ss1 + ss0

            if reverse:
                ss = [s[::-1] for s in ss]
                self.assert_no_bad_histories(histories)
            histories += ss

        return histories

    def assert_no_bad_histories(self, histories):
        has_legal = False
        has_ilegal = False
        for hist in histories:
            # might be orphan wich is OK
            if not hist.startswith('r'):
                has_ilegal = True
            # means it has a normal ancestry
            if hist.startswith('r'):
                has_legal = True
        if has_ilegal and has_legal:
            assert False, "this node has a bug - both a history which goes to root, " \
                          "and a history which is redundant"

    def get_branch_represantation(self):
        # always go first to truth child (repr. by t), and if last - return value (0 or 1)
        if self.is_last:
            return str(self.value)
        else:
            s = ''
            s += 't'
            s += self.true_child.get_branch_represantation()
            s += 'f'
            s += self.false_child.get_branch_represantation()
        return s

#=====================================================================
#   class tree: 
#=====================================================================
class Tree(object):
    def __init__(self, logical_expression):
        self.logical_expression = logical_expression
        self.truth_table, self.layer_amount = expression_to_truth_table(self.logical_expression)
        #self.layer_amount = layer_amount
        self.root = Node(None, 0, 0)
        self.layers = []
        # +1 for values layer (truth table)
        for i in range(self.layer_amount + 1):
            self.layers.append([])
        self.layers[0] = [self.root]
        self.build_tree()
        self.input_final_values()

    def build_tree(self):
        # Don't want to build children for last layer - which is the values layer...
        for layer in self.layers[:-1]:
            for j, node in enumerate(layer):
                self._build_children(node, j)

    def _build_children(self, node, j):
        child_layer = node.layer_num + 1
        node.false_child = Node([node], child_layer, index=2*j)
        node.true_child = Node([node], child_layer, index=2*j + 1)
        self.layers[child_layer] += [node.false_child, node.true_child]

    def input_final_values(self):
        for node in self.layers[-1]:
            node.is_last = True
            node.value = self.truth_table[node.index]
#this is the print we were asignd to do
    def _print_layer(self, layer_num):
        current_layer = self.layers[layer_num]
        for node in current_layer:
            node.print_node()
        print ()
#this is a better looking print.
    def _print_layer2(self, layer_num):
        current_layer = self.layers[layer_num]
        for node in current_layer:
            node.print_node2()
            print('\t', end='')

    def print_tree(self):
        for layer_num in range(self.layer_amount + 1):
            self._print_layer(layer_num)
        #for layer_num in range(self.layer_amount + 1):
        #    self._print_layer2(layer_num)
        #    print()
    def remove_redundant_test(self):
        for layer in self.layers:
            for node in layer:
                if not node.parents:
                    continue
                if node.is_last:
                    continue
                if node.true_child == node.false_child:
                    for parent in node.parents:
                        if parent.true_child == node:
                            parent.true_child = node.true_child
                        if parent.false_child == node:
                            parent.false_child = node.true_child
                        if parent not in node.true_child.parents:
                            node.true_child.parents.append(parent)
                    node.true_child.parents.remove(node)
                    node.parents = None
        self.remove_orphans()

    def merge_equivalent_branches(self):
        found_something = True
        while found_something:
            self.remove_orphans()
            found_something = False

            for layer in self.layers:
                reprs = [n.get_branch_represantation() for n in layer]
                for node in layer:
                    # if node doesn't have parents - it is either root, or an orphan and not interesting
                    if node.parents is None:
                        continue
                    this_repr = node.get_branch_represantation()
                    if reprs.count(this_repr) > 1:
                        found_something = True
                        # taking [1] for second instance with same branch architecture.
                        # we will always have node be the first one, and other_node be second one
                        other_node = [n for n in layer if n.get_branch_represantation() == this_repr][1]
                        for parent in node.parents:
                            if parent.false_child is node:
                                parent.false_child = other_node
                            # Not using else because it might be true for both
                            if parent.true_child is node:
                                parent.true_child = other_node
                            # To prevent having same father twice
                            if parent not in other_node.parents:
                                other_node.parents.append(parent)
                        node.parents = None
                        if node.true_child:
                            node.true_child.parents.remove(node)
                        if node.false_child:
                            node.false_child.parents.remove(node)
                        break
                if found_something:
                    break
        self.remove_orphans()

    def remove_orphans(self):
        for layer in self.layers:
            to_remove = []
            for node in layer:
                if node.is_orphan():
                    to_remove.append(node)
            for n in to_remove:
                layer.remove(n)


# main
if __name__ == "__main__":
    print ("\n\n")
    print ("==========================================================================================")
    expr1 = "(~a&~b)|(~a&b&~c)|(a&b&~c)"              #as it was requested
    print ("this is the expretion:\t{exp}".format(exp=expr1))
    t1 = Tree(expr1)                   #bulding the basic tree    
    print ("this is the basic tree:")
    t1.print_tree() 
    print ()
    t1.merge_equivalent_branches()     #merge branches
    t1.remove_redundant_test()         #remode redundent nodes
    print ("\nthe final tree:")
    t1.print_tree()

    print ('\n\n')
    #expr2 = "(a|c)&(~b|c)"              #as it was requested    
    #print ("this is the expretion:\t{exp}" .format(exp=expr2))    
    #t2 = Tree(expr2)
    #print ("this is the basic tree:")
    #t2.print_tree()
    #t2.merge_equivalent_branches()
    #t2.remove_redundant_test()
    #print ("\nthe final tree:")
    #t2.print_tree()
    
    #if you want to see the logic expression after iterations
    #print (t1.layers[-1][0].get_histories())
    print ("logic expression after iterations:")
    print (t1.layers[-1][1].get_histories())  
    #print (t2.layers[-1][0].get_histories())
    #print (t2.layers[-1][1].get_histories())  
    print ("==========================================================================================")

    print ("\n\n\n")    
    
    
    
    
    
    
    
    
    
    
    
    
    